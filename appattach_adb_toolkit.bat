@echo off
mode con:cols=80 lines=50
set scriptpath=%~d0%~p0
:begin
cls

COLOR 0A
ECHO --------------------------------------------------------------------------------
ECHO appAttach ADB Toolkit                              %date% -- %time%
ECHO --------------------------------------------------------------------------------
ECHO Script path: %scriptpath%
ECHO --------------------------------------------------------------------------------

ECHO                            http://www.appattach.com
echo ================================================================================
adb -d devices
ECHO --------------------------
if %errorlevel% == 9009 goto :noadb


echo.
echo Select a task:
echo ==============
echo.
echo Suggested Apps
echo --------------
echo a1. Enable debug logging
echo a2. Refresh widget offers
echo. 
echo Device Manager
echo --------------
echo b1. Enable debug logging
echo b2. Simulate first boot
echo b3. Reload homescreen shortcut offers
echo.
echo InsideApps SDK (Included in DeviceManager 3.1.4 and newer)
echo --------------
echo c1. Accelerate reporting to hourly
echo c2. Disable SDK Logging
echo c3. Disable SDK
echo c4. Force inventory report
echo c5. Send all queued activity
echo.

echo General
echo --------------
echo 1. Run automatic check
echo 2. Generate bugreport
echo 3. Generate logcat
echo 4. Install ADB
echo 5. Export /system file list
echo 6. View test tracker
echo 0. Exit
echo.

rem CHOICE /C 123456789abcdefghijklmnopqrstuvwxyz /N /M "Enter your choice:"
rem if not '%choice%'== set choice=%choice:~0,1%
rem set choice=%ERRORLEVEL%
SET /P choice=Choose an option:
goto :choice_%choice%

echo "%choice%" is not a valid option. Please try again.
goto begin


:choice_a1
cls
echo Enable/disable debug logging for Suggested Apps:
echo =====================
echo.
echo This option will turn on debug logging within Suggested Apps. 
echo If debug is on then Suggested Apps will be much more verbose in its messaging. 
echo This can be seen by looking at the device's logcat output. 
echo.
echo Select a task:
echo ==============
echo 1. Enable debug logging
echo 2. Disable debug logging
echo 3. Go Back
echo. 
CHOICE /C 123 /N /M "Enter your choice:"
set choice=%ERRORLEVEL%
goto :choice_a1_%choice%

:choice_a1_1
cls
adb -d shell "am broadcast -a DEBUG_ON -n com.appattach.suggestedapps/com.appattach.suggestedapps.WidgetProvider"
pause
goto begin

:choice_a1_2
cls
adb -d shell "am broadcast -a DEBUG_OFF -n com.appattach.suggestedapps/com.appattach.suggestedapps.WidgetProvider"
pause
goto begin

:choice_a1_3
goto begin



:choice_a2
cls
adb -d shell "am broadcast -a reload -n com.appattach.suggestedapps/com.appattach.suggestedapps.WidgetProvider"
pause
goto begin


:choice_b1
cls
echo Enable/disable debug logging for DeviceManager:
echo =====================
echo.
echo This option will turn on debug logging within DeviceManager. 
echo If debug is on then DeviceManager will be much more verbose in its messaging. 
echo This can be seen by looking at the device's logcat output. 
echo.
echo Select a task:
echo ==============
echo 1. Enable debug logging
echo 2. Disable debug logging
echo 3. Go Back
echo. 
CHOICE /C 123 /N /M "Enter your choice:"
set choice=%ERRORLEVEL%
goto choice_b1_%choice%

:choice_b1_1
cls
adb -d shell "am broadcast -a DEBUG_ON -n com.devicemanager/com.devicemanager.Receiver"
pause
goto begin

:choice_b1_2
cls
adb -d shell "am broadcast -a DEBUG_OFF -n com.devicemanager/com.devicemanager.Receiver"
pause
goto begin


:choice_b1_3
goto begin


:choice_b2
cls
adb -d shell "am broadcast -a android.intent.action.BOOT_COMPLETED -n com.devicemanager/.Receiver"
pause
goto begin

:choice_b3
cls
adb -d shell "am broadcast -a LOAD_OFFERS -n com.devicemanager/com.devicemanager.Receiver -e folder /system/etc/offers/"
pause
goto begin


:choice_c1
cls
adb -d shell "am broadcast -a SDK:SET -n com.devicemanager/.Receiver -e pref long,activityRate,3600"
pause
goto begin


:choice_c2
cls
adb -d shell "am broadcast -a SDK:SET -n com.devicemanager/.Receiver -e pref boolean,debug,false"
pause
goto begin


:choice_c3
cls
adb -d shell "am broadcast -a SDK:SET -n com.devicemanager/.Receiver -e pref string,active,false"
pause
goto begin


:choice_c4
cls
adb -d shell "am broadcast -a INVENTORY -n com.devicemanager/.Receiver"
pause
goto begin


:choice_c5
cls
adb -d shell "am broadcast -a ACTIVITY -n com.devicemanager/.Receiver"
pause
goto begin



:choice_1
cls
echo.
echo.

set devicemanager_apk=0
echo Checking for the devicemanager APK...
echo -------------------------------------
for /f "tokens=1" %%i in ('adb shell ls /system/app/ ^| findstr "DeviceManager_ com.appattach.devicemanager.apk"') do (
	set devicemanager_apk=1
	set filename=%%i
	echo SUCCESS: the Device Manager APK was found. Filename: %filename%
)
if %devicemanager_apk% == 1 (
echo *note that this only checks for any filename containing "DeviceManager_" or "com.appattach.devicemanager.apk"
echo *and does not prove that you have the correct version.
echo *You may also see a false positive from any other APK that has "DeviceManager_" in its name.
)
if %devicemanager_apk% == 0 (
echo ERROR:
echo the Device Manager APK was not found in the /system/app directory. 
echo This is a big problem.
)

echo.
echo.

set devicemanager_cfg=0
echo Checking for the devicemanager Config file (devicemanager.cfg)...
echo -----------------------------------------------------------------
for /f "tokens=1" %%i in ('adb shell ls /system/etc/ ^| findstr "devicemanager.cfg"') do ^
set devicemanager_cfg=1
if %devicemanager_cfg% == 1 (
echo SUCCESS: devicemanager.cfg was found. 
echo Saving contents
adb pull /system/etc/devicemanager.cfg
echo devicemanager.cfg exported to %scriptpath%devicemanager.cfg
)

if %devicemanager_cfg% == 0 (
echo ERROR: 
echo devicemanager.cfg was not found in the /system/etc directory. 
echo This is a big problem.
)

echo.
echo.

pause
goto begin




:choice_2
cls
echo Generate Bugreport:
echo =====================
echo Please wait, this may take some time...
adb -d bugreport > bugreport.txt
echo bugreport saved to %scriptpath%bugreport.txt
echo This file contains information about the device which may be helpful for troubleshooting.
pause
goto begin

:choice_3
cls
echo Generate Logcat:
echo =====================
echo Please wait, this may take some time...
adb -d logcat -d -v long *:V > logcat.txt
echo.
echo -----------------------------------------------------------------
echo logcat saved to %scriptpath%logcat.txt
echo This file contains information about the device which may be helpful for troubleshooting.
echo -----------------------------------------------------------------
echo.
pause
goto begin


:choice_4
cls
bitsadmin.exe /transfer "adb setup" http://www.mediafire.com/download/bys33r1e57dt9na/adb-setup-1.3.exe "%scriptpath%adb-setup.exe"
echo bitsadmin.exe /transfer "adb setup" http://www.mediafire.com/download/bys33r1e57dt9na/adb-setup-1.3.exe "%scriptpath%adb-setup.exe"
pause
echo Download operation complete. the ADB installer is at %scriptpath%adb-setup.exe Press any key to attempt to begin installation for you.
"%scriptpath%adb-setup.exe"
pause
goto begin



:choice_5
cls
echo Generate /system file list:
echo =====================
echo Please wait, this may take some time...
adb -d shell ls -R /system/ > system.txt
echo.
echo -----------------------------------------------------------------
echo file list saved to %scriptpath%system.txt
echo This file contains information about the device which may be helpful for troubleshooting.
echo -----------------------------------------------------------------
echo.
pause
goto begin



:choice_6
cls
for /f "tokens=1,2,3,4,5" %%i in ('adb shell netcfg ^| findstr "wlan0"') do ^
set devicemac=%%m

if not [%devicemac%] == [] (
echo MAC address found. 
echo We will now open your browser to the test tracker page for this device.
)

if [%devicemac%] == [] (
echo MAC address not found. 
echo We will now open your browser to the test tracker page where you can search for your device.
)

echo.
echo ==================================
echo When you are asked for credentials
echo ----------------------------------
echo username: testing                
echo password: dmTesting              
echo ==================================
echo.

pause
start http://devmgr.net/testing?filter=%devicemac%
goto begin


:choice_0
exit

:noadb
echo *********************************************************************************
echo ADB NOT ACCESSIBLE. THIS UTILITY REQUIRES ADB TO COMMUNICATE WITH YOUR DEVICE. 
echo PLEASE ENSURE THAT ADB IS INSTALLED AND IN THE "PATH". 
echo This script can attempt to install ADB for you.
echo *********************************************************************************
echo.
CHOICE /M "Would you like this script to attempt automated installation?"

if %errorlevel% == 1 goto choice_4

if %errorlevel% == 2 ( 
	echo come back when ADB is installed
	goto end
)




:end
pause
exit
