# appAttach ADB Toolkit #

The appAttach ADB Toolkit a basic GUI for running common ADB operations for DeviceManager and Suggested Apps. It is intended as a time-saving tool for our internal testing and for distribution to our partners to simplify troubleshooting and reporting for them. As it is only a GUI, it depends on ADB and drivers already being installed and configured. If ADB is not detected on the system, the toolkit will offer to attempt to install ADB for them.

